import people_data from '../data/people_data.json';

export class PeopleProcessing {
    getById(id: number) {
        return people_data.find((p) => p.id === id);
    }

    getAll(page: number, pageSize: number) {
        const pagedPeople = people_data.slice(page*pageSize, (page+1)*pageSize)
        return {
            list: pagedPeople,
            total: people_data.length,
        };
    }
}
