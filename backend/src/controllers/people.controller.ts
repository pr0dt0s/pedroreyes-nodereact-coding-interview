import {
    JsonController,
    Get,
    HttpCode,
    NotFoundError,
    Param,
    QueryParam
} from 'routing-controllers';
import { PeopleProcessing } from '../services/people_processing.service';

const peopleProcessing = new PeopleProcessing();

@JsonController('/people', { transformResponse: false })
export default class PeopleController {
    @HttpCode(200)
    @Get('/all')
    getAllPeople(
        @QueryParam('page') page: number = 0,
        @QueryParam('pageSize') pageSize: number = 32,
        @QueryParam('queryString') queryString: string
    ) {

        const queries = queryString.split(' ');
        const filterFunctions = queries.map(query => {
            const [attribute, text] = query.split(':');
            return (data:) => 
        })

        const people = peopleProcessing.getAll(page, pageSize);

        if (!people) {
            throw new NotFoundError('No people found');
        }

        return {
            data: people,
        };
    }

    @HttpCode(200)
    @Get('/:id')
    getPerson(@Param('id') id: number) {
        const person = peopleProcessing.getById(id);

        if (!person) {
            throw new NotFoundError('No person found');
        }

        return {
            data: person,
        };
    }
}
